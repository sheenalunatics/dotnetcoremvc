﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using PartyInvites.Models;

namespace PartyInvites.Controllers
{
    public class HomeController : Controller
    {
        public IRepository Repository = SimpleRepository.SharedRepository;
        //[HttpGet]
        //public ViewResult Login()
        //{
        //    return View();
        //}

        //[HttpPost]
        //public ViewResult Login(LoginModel model)
        //{
        //    if (ModelState.IsValid) {
        //        //if (model.Username.ToLower().Equals("ukrit.s") && (model.Password.ToLower().Equals("yes"))) {
        //        model.id = Repository.Logins.Count() > 0 ? Repository.Logins.Count() + 1 : 1;
        //        Repository.AddLogin(model);
        //        return View("Thanks", model);
        //        //}
        //    }
        //    return View();
        //}

        //public ViewResult ListLogins() {
        //    return View(Repository.Logins);
        //}

        public IActionResult Index() => View(Repository.Products);

        [HttpGet]
        public IActionResult AddProduct() => View();

        [HttpPost]
        public IActionResult AddProduct(Product p) {
            Repository.AddProduct(p);
            return RedirectToAction("Index");
        }

        public IActionResult About()
        {
            ViewData["Message"] = "Your application description page.";

            return View();
        }

        public IActionResult Contact()
        {
            ViewData["Message"] = "Your contact page.";

            return View();
        }

        public IActionResult Error()
        {
            return View(new ErrorViewModel { RequestId = Activity.Current?.Id ?? HttpContext.TraceIdentifier });
        }
    }
}
